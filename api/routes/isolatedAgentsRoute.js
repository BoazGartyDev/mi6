'use strict';
module.exports = function (app) {
    var agentsController = require('../controllers/isolatedAgentsController');

    app.route('/countries-by-isolation/')
        .get(agentsController.isolated) // read specific entry from database.

    app.route('/find-closest/')
        .post(agentsController.find_closest) // read specific entry from database.


};
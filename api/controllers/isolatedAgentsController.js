'use strict';

var distance = require('google-distance-matrix');
const wrap = require("../wrap");
var _this = this;
var exports = module.exports = {};

let sampleData = [
    {
        agent: '007', country: 'Brazil',
        address: 'Avenida Vieira Souto 168 Ipanema, Rio de Janeiro',
        date: 'Dec 17, 1995, 9:45:17 PM'
    },
    {
        agent: '005', country: 'Poland',
        address: 'Rynek Glowny 12, Krakow',
        date: 'Apr 5, 2011, 5:05:12 PM'
    },
    {
        agent: '007', country: 'Morocco',
        address: '27 Derb Lferrane, Marrakech',
        date: 'Jan 1, 2001, 12:00:00 AM'
    },
    {
        agent: '005', country: 'Brazil',
        address: 'Rua Roberto Simonsen 122, Sao Paulo',
        date: 'May 5, 1986, 8:40:23 AM'
    },
    {
        agent: '011', country: 'Poland',
        address: 'swietego Tomasza 35, Krakow',
        date: 'Sep 7, 1997, 7:12:53 PM'
    },
    {
        agent: '003', country: 'Morocco',
        address: 'Rue Al-Aidi Ali Al-Maaroufi, Casablanca',
        date: 'Aug 29, 2012, 10:17:05 AM'
    },
    {
        agent: '008', country: 'Brazil',
        address: 'Rua tamoana 418, tefe',
        date: 'Nov 10, 2005, 1:25:13 PM'
    },
    {
        agent: '013', country: 'Poland',
        address: 'Zlota 9, Lublin',
        date: 'Oct 17, 2002, 10:52:19 AM'
    },
    {
        agent: '002', country: 'Morocco',
        address: 'Riad Sultan 19, Tangier',
        date: 'Jan 1, 2017, 5:00:00 PM'
    },
    {
        agent: '009', country: 'Morocco',
        address: 'atlas marina beach, agadir',
        date: 'Dec 1, 2016, 9:21:21 PM'
    }

]
exports.isolated = wrap(async (req, res) => {

    // can get data from webs-service or hard-coded in this file.
    let missions = req.query.data;
    if (missions) {
        missions = JSON.parse(missions);
    } else {
        missions = sampleData;
    }
    if (!missions || missions.length == 0) {
        // in production there should be a model object for error result.
        return res.json({ "Error": "Empty agents missions data" });
    }
    // Map of all agents, Key=agent number
    let agentsHash = new Map();

    // Map of isolated agents by country, key=country
    let isolatedHash = new Map();
    // global Max
    let max = Number.MIN_SAFE_INTEGER;
    // global max - country name
    let maxCountry;
    for (let i = 0; i < missions.length; i++) {
        let agent = agentsHash.get(missions[i].agent)

        if (agent) {
            // if agent already on the list change isolated to false.            
            agent['isolated'] = false;
        }
        // add agent to map if not exist.
        else {
            agentsHash.set(missions[i].agent, { "country": missions[i]['country'], "isolated": true });
        }
    }
    // after crating isolated agents array - find country with max isolated agents.      
    for (const [key, value] of agentsHash.entries()) {

        // if Isolated add to isolated countries map, update local max and check global Max.
        if (value['isolated'] == true) {

            // check number of agents in curr country if exist.
            let currNumOfAgents = isolatedHash.get(value['country']);
            console.log("isloated " + JSON.stringify(key) + " " + JSON.stringify(value));
            if (currNumOfAgents) {
                // if has value update local agent number and check for global Max.
                currNumOfAgents++;
                isolatedHash.set(value['country'], currNumOfAgents);
                if (currNumOfAgents > max) {
                    max = currNumOfAgents;
                    maxCountry = value['country'];
                }
            }
            else {
                // first agent in this current country add to hash.
                if (max < 1) {
                    max = 1;
                    maxCountry = value['country'];
                }
                // add element to isolated by country and save counter for the isolated agent, starts with 1.
                isolatedHash.set(value['country'], 1);
            }

        }
    }

    // if no isolated agents - show error.
    if (isolatedHash.size == 0) {
        return res.json({ "Error": "No isolated agents" });
    }
    //for simplicity return object inline, in production need to use a response object model using factory.
    console.log(JSON.stringify({ 'maxAgents': max, 'country': maxCountry }));
    return res.json({ 'maxAgents': max, 'country': maxCountry });

});

exports.find_closest = wrap(async (req, res) => {
    // init max and min 
    let min = Number.MAX_SAFE_INTEGER;
    let max = Number.MIN_SAFE_INTEGER;
    var farthest;
    let closest;
    // get target location and  missions from web-service
    let targetLocation = req.query.target_location;
    let missions = req.query.data;
    if (missions) {
        missions = JSON.parse(missions);
    } else {
        missions = sampleData;
    }
    // loop through all the location and find max and min distance.
    if (!missions || missions.length == 0) {
        return res.json({ "Error": "Destinations list is empty" });
    }
    let origins = [];
    let destinations = [];
    // copy address from missions array. google API will calculate the distances matrix
    missions.forEach(function (curr) {
        destinations.push(curr["address"])

    })

    // add origin to origins array - (can work with multiple origins as well).
    origins.push(targetLocation);
    distance.key('AIzaSyC9b5ApOCBSsO-Nyz30qpPagliRokzKJyo');
    distance.units('metric');
    distance.matrix(origins, destinations, function (err, distances) {
        if (err) {
            return console.log(err);
        }
        if (!distances) {
            return res.json({ "Error": "destination list is empty" });
        }
        // loop through destination and origin result and find max and min locations.
        // the result are by land only in google maps - places without land connecting between them would not work (like Tel Aviv to brazil).
        if (distances.status == 'OK') {
            for (let i = 0; i < origins.length; i++) {
                for (let j = 0; j < destinations.length; j++) {
                    let destination = await distances.destination_addresses[j];
                    if (distances.rows[0].elements[j].status == 'OK') {
                        let currDistance = distances.rows[i].elements[j].distance['value']
                        // check if update min/max is needed.
                        if (currDistance > max) {

                            max = currDistance;
                            farthest = destination;
                        }
                        if (currDistance < min) {
                            closest = destination;
                            min = currDistance
                        }
                    }
                }
            }
            //for simplicity return object inline, in production need to use a response object model using factory.
            return res.json({ "farthest": { "address": farthest, "distance": max }, "closest": { "address": closest, "distance": min } })
        }
    });
    //return findClosest(req, res);

});